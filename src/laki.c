#include "laki.h"

/*
Copyright (c) 2022-2023 Devine Lu Linvega, Andrew Alderwick, Andrew Richards
Copyright (c) 2024 Dave Van Ee

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define IMM { if(_s) { PU2(ram[pc++] << 8 | ram[pc++]) } else { PU1(ram[pc++]) } }
#define INC(s) s->dat[s->ptr++]
#define DEC(s) s->dat[--s->ptr]
#define JMx(x) { if(_2) JM2(x) else JM1(x) }
#define JM2(x) { if(_a) pc = (x); else pc += (x); }
#define JM1(x) { if(_a) { tt = ram[x++] << 8; pc = tt | ram[(Uint8)x]; } else { pc += (Sint8)x; } }
#define POx(o) { if(_2) { PO2(o) } else PO1(o) }
#define PO2(o) PO1(o) o |= (_r ? DEC(rs) : DEC(ws)) << 8;
#define PO1(o) o = _r ? DEC(rs) : DEC(ws);
#define PUx(y) { if(_2) { PU2(y) } else PU1(y) }
#define PU2(y) { tt = (y); PU1(tt >> 8) PU1(tt) }
#define PU1(y) { if(_r) INC(rs) = y; else INC(ws) = y; }
#define PFx(y) { if(_2) { PF2(y) } else PF1(y) }
#define PF2(y) { tt = (y); PF1(tt >> 8) PF1(tt) }
#define PF1(y) { if(_r) INC(ws) = y; else INC(rs) = y; }
#define LEV(p, o) { if(_2) { o = (emu_lev(u, p) << 8) | emu_lev(u, p + 1); } else o = emu_lev(u, p); }
#define SEV(p, y) { if(_2) { emu_sev(u, p, y >> 8), emu_sev(u, p + 1, y); } else emu_sev(u, p, y); }
#define PEK(o, x, r) { if(_2) { r = (x); o = ram[r++] << 8 | ram[r]; } else o = ram[(x)]; }
#define POK(x, y, r) { if(_2) { r = (x); ram[r++] = y >> 8, ram[r] = y; } else ram[(x)] = (y); }
#define PEZ(o, x, r) { if(_2) { r = (x); o = ram[u->ib + r++] << 8 | ram[u->ib + r]; } else o = ram[u->ib + (x)]; }
#define POZ(x, y, r) { if(_2) { r = (x); ram[u->ib + r++] = y >> 8, ram[u->ib + r] = y; } else ram[u->ib + (x)] = (y); }

#define OPRI(opc, bim, sim, init, body) {\
	case 0x00|opc: {enum{_2=0,_r=0}; init; body; break;}\
	case 0x20|opc: {enum{_2=1,_r=0}; init; body; break;}\
	case 0x40|opc: {enum{_2=0,_r=1}; init; body; break;}\
	case 0x60|opc: {enum{_2=1,_r=1}; init; body; break;}\
	case 0x80|opc: {enum{_2=0,_r=0,_s=bim}; IMM init; body; break;}\
	case 0xa0|opc: {enum{_2=1,_r=0,_s=sim}; IMM init; body; break;}\
	case 0xc0|opc: {enum{_2=0,_r=1,_s=bim}; IMM init; body; break;}\
	case 0xe0|opc: {enum{_2=1,_r=1,_s=sim}; IMM init; body; break;}\
}

#define OPKR(opc, init, body) {\
	case 0x00|opc: {enum{_2=0,_r=0}; init; body; break;}\
	case 0x20|opc: {enum{_2=1,_r=0}; init; body; break;}\
	case 0x40|opc: {enum{_2=0,_r=0}; k = ws->ptr; init; ws->ptr = k; body; break;}\
	case 0x60|opc: {enum{_2=1,_r=0}; k = ws->ptr; init; ws->ptr = k; body; break;}\
	case 0x80|opc: {enum{_2=0,_r=1}; init; body; break;}\
	case 0xa0|opc: {enum{_2=1,_r=1}; init; body; break;}\
	case 0xc0|opc: {enum{_2=0,_r=1}; k = rs->ptr; init; rs->ptr = k; body; break;}\
	case 0xe0|opc: {enum{_2=1,_r=1}; k = rs->ptr; init; rs->ptr = k; body; break;}\
}

#define OPKI(opc, bim, sim,  init, body) {\
	case 0x00|opc: {enum{_2=0,_r=0}; init; body; break;}\
	case 0x20|opc: {enum{_2=1,_r=0}; init; body; break;}\
	case 0x40|opc: {enum{_2=0,_r=0}; k = ws->ptr; init; ws->ptr = k; body; break;}\
	case 0x60|opc: {enum{_2=1,_r=0}; k = ws->ptr; init; ws->ptr = k; body; break;}\
	case 0x80|opc: {enum{_2=0,_r=0,_s=bim}; IMM init; body; break;}\
	case 0xa0|opc: {enum{_2=1,_r=0,_s=sim}; IMM init; body; break;}\
	case 0xc0|opc: {enum{_2=0,_r=0,_s=bim}; k = ws->ptr; IMM init; ws->ptr = k; body; break;}\
	case 0xe0|opc: {enum{_2=1,_r=0,_s=sim}; k = ws->ptr; IMM init; ws->ptr = k; body; break;}\
}

#define OPAI(opc, bim, sim, init, body) {\
	case 0x00|opc: {enum{_2=0,_r=0,_a=0}; init; body; break;}\
	case 0x20|opc: {enum{_2=1,_r=0,_a=0}; init; body; break;}\
	case 0x40|opc: {enum{_2=0,_r=0,_a=1}; init; body; break;}\
	case 0x60|opc: {enum{_2=1,_r=0,_a=1}; init; body; break;}\
	case 0x80|opc: {enum{_2=0,_r=0,_a=0,_s=bim}; IMM init; body; break;}\
	case 0xa0|opc: {enum{_2=1,_r=0,_a=0,_s=sim}; IMM init; body; break;}\
	case 0xc0|opc: {enum{_2=0,_r=0,_a=1,_s=bim}; IMM init; body; break;}\
	case 0xe0|opc: {enum{_2=1,_r=0,_a=1,_s=sim}; IMM init; body; break;}\
}

int
run_vector(Laki *u, Uint16 pc, bool halt) {
	u->s = &u->wst;
	u->pc = pc;
	u->fault = 0;
	u->vector_active = true;
	run_laki(u, 0, true, halt);
	/* TODO: Where do we handle single stepping and debugger redraw? */
	return 1;
}

int
run_laki(Laki *u, unsigned int steps, bool initial, bool halt)
{
	unsigned int res, stepCount = steps ? steps : 100000;
	if(initial && !u->pc) goto completed;
	for(;;) {
		res = laki_eval(u, stepCount, halt);
		if(u->fault) break;
		/* Do... mid-vector handling stuff? */
		if(steps) goto residual;
	}
	if(u->fault == 2) {
		u->fault = 0;
		u->running = false;
		goto residual;
	}
completed:
	if(u->running) u->fault = 0;
	u->vector_active = false;
residual:
	return 1;
}

int
laki_eval(Laki *u, unsigned int limit, bool halt)
{
	int a,b,c,k;
	Uint8 t;
	Uint16 tt, pc = u->pc;
	Uint8 *ram = u->ram;
	Stack *ws = u->s, *rs = (ws == &u->wst) ? &u->rst : &u->wst, *_ts;
	if(!pc || (halt && u->dev[0x0f])) return 0;
	while(limit) {
		limit--;
		switch(ram[pc++]) {
		/* BRK */ case 0x00: u->fault = 1; goto done;
		/* RET */ case 0x20: tt = DEC(rs); pc = tt | (DEC(rs) << 8); break;
		/* FLP */ case 0x40: _ts = rs; rs = ws; ws = _ts; break;
		/* SIB */ case 0x60: tt = DEC(ws); u->ib = tt | (DEC(ws) << 8); break;
		/* PS2 */ case 0xa0: INC(ws) = ram[pc++];
		/* PSH */ case 0x80: INC(ws) = ram[pc++]; break;
		/* P2r */ case 0xe0: INC(rs) = ram[pc++];
		/* PSr */ case 0xc0: INC(rs) = ram[pc++]; break;
		/* POP */ OPRI(0x01, 0, 1, POx(a), {})
		/* NIP */ OPRI(0x02, 0, 1, POx(a) POx(b), PUx(a))
		/* SWP */ OPRI(0x03, 0, 1, POx(a) POx(b), PUx(a) PUx(b))
		/* DIG */ OPKR(0x04, POx(a) POx(b) POx(c), PUx(b) PUx(a) PUx(c))
		/* DUP */ OPRI(0x05, 0, 1, POx(a), PUx(a) PUx(a))
		/* OVR */ OPRI(0x06, 0, 1, POx(a) POx(b), PUx(b) PUx(a) PUx(b))
		/* STH */ OPKR(0x07, POx(a), PFx(a))
		/* LAB */ OPKR(0x08, PO2(a), PEK(b, a, tt) PUx(b))
		/* SAB */ OPKR(0x09, PO2(a) POx(b), POK(a, b, tt))
		/* LOF */ OPRI(0x0a, 0, 0, PO1(a), PEK(b, pc + (Sint8)a, tt) PUx(b))
		/* SOF */ OPRI(0x0b, 0, 0, PO1(a) POx(b), POK(pc + (Sint8)a, b, tt))
		/* LAZ */ OPKI(0x0c, 0, 0, PO1(a), PEZ(b, a, t) PUx(b))
		/* SAZ */ OPKI(0x0d, 0, 0, PO1(a) POx(b), POZ(a, b, t))
		/* LEV */ OPKI(0x0e, 0, 0, PO1(a), LEV(a, b) PUx(b))
		/* SEV */ OPKI(0x0f, 0, 0, PO1(a) POx(b), SEV(a, b) if(u->fault) goto done; )
		/* ADD */ OPKI(0x10, 0, 1, POx(a) POx(b), PUx(b + a))
		/* SUB */ OPKI(0x11, 0, 1, POx(a) POx(b), PUx(b - a))
		/* MUL */ OPKI(0x12, 0, 1, POx(a) POx(b), PUx(b * a))
		/* DIV */ OPKI(0x13, 0, 1, POx(a) POx(b), PUx(a ? b / a : 0))
		/* AND */ OPKI(0x14, 0, 1, POx(a) POx(b), PUx(b & a))
		/* BOR */ OPKI(0x15, 0, 1, POx(a) POx(b), PUx(b | a))
		/* XOR */ OPKI(0x16, 0, 1, POx(a) POx(b), PUx(b ^ a))
		/* SFT */ OPKI(0x17, 0, 0, PO1(a) POx(b), PUx(b >> (a & 0xf) << (a >> 4)))
		/* INC */ OPKR(0x18, POx(a), PUx(a + 1))
		/* DEC */ OPKR(0x19, POx(a), PUx(a - 1))
		/* EQU */ OPKI(0x1a, 0, 1, POx(a) POx(b), PU1(b == a))
		/* GTH */ OPKI(0x1b, 0, 1, POx(a) POx(b), PU1(b > a))
		/* LTH */ OPKI(0x1c, 0, 1, POx(a) POx(b), PU1(b < a))
		/* JMP */ OPAI(0x1d, 0, 1, POx(a), JMx(a))
		/* JIF */ OPAI(0x1e, 0, 1, POx(a) PO1(b), if(b) JMx(a))
		/* JUB */ OPAI(0x1f, 0, 1, POx(a), PF2(pc) JMx(a))
		}
	}
done:
	u->pc = pc;
	u->s = ws;
	return limit;
}
