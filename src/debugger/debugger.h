#ifndef DEBUGGER_H
#define DEBUGGER_H

#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_sdlrenderer2.h"
#include "imgui_memory_editor.h"
#include "imgui_stack_viewer.h"
#include "imgui_program_editor.h"
#include "imgui_debug_control.h"
#include "../laki.h"

void debugger_init(void);
void debugger_process_frame(Laki *u);
void debugger_shutdown(void);

#endif

